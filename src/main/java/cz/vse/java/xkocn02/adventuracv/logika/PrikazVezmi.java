package cz.vse.java.xkocn02.adventuracv.logika;

/**
 *  Třída PrikazVezmi implementuje pro hru příkaz vezmi.
 *  Tato třída je součástí jednoduché textové hry.
 *  
 *@author     Nikoleta Kocáková
 *@version    v1.0 pre semestrálnu prácu.
 */

public class PrikazVezmi implements IPrikaz{
    private static final String NAZEV = "vezmi";
    private HerniPlan plan;
    private Batoh batoh;
    
    /**
     *  Konstruktor třídy
     *  
     *  @param plan herní plán, ve kterém se bude ve hře "chodit"
     *  @param batoh batoh , ve kterem se nacházejí předmety.
     */    
     public PrikazVezmi(HerniPlan plan , Batoh batoh) {
         this.plan = plan;
         this.batoh = batoh;
     }
     
     /**
     *  Provádí příkaz "vezmi". Zkouší se sebrat daný předmět. Pokud předmět
     *  existuje, přidá se předmět do batohu. Pokud zadaný předmět
     *  není, vypíše se chybové hlášení.
     *
     *@param parametry - jako  parametr obsahuje jméno ppředmětu,
     *                         který se má vzít.
     *@return zpráva, kterou vypíše hra hráči
     */ 
     @Override
     public String provedPrikaz(String... parametry) {
         if (parametry.length == 0) {
             // pokud chybí druhé slovo (sousední prostor), tak ....
             return "Čo mám zobrať? Musíš zadat jméno veci";
         }

         String vezmiTentoPredmet = parametry[0];

         // skúšame zobrať nasledujúcu vec do batohu
         Predmet predmet = plan.getAktualniProstor().vratPredmet(vezmiTentoPredmet);
         
         
         if(plan.getAktualniProstor().obsahujePredmet(predmet) == false){
             return "Predmet " + vezmiTentoPredmet +" sa v miestnosti nenachádza.";
         }
         if(batoh.getSize() >= 3){
             return "Batoh je plný.";
         }
         if(plan.getAktualniProstor().obsahujePredmet(predmet) == false){
             return "Predmet sa v miestnosti nenachádza.";
         }
         if(predmet.getNazov() == "babička" && plan.getKyslikPoskytnuty() == false) {
             return "Aby ste babičku mohli zachrániť , potrebujete jej najprv poskytnúť kyslík.";
         }
         if(predmet.getPrenositelny() == false){
             return "Tento predmet nemôžete prenáša";
         }
         
         
         plan.getAktualniProstor().odoberPredmet(predmet);
         batoh.pridajPredmet(predmet);
         return "Predmet úspešne pridaný do batohu:" + batoh.getObsahBatohu()  ;
         
     }
     
     
     /**
      *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
      *  
      *  @ return nazev prikazu
      */
     @Override
     public String getNazev() {
         return NAZEV;
     }
}
