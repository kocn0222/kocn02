package cz.vse.java.xkocn02.adventuracv.logika;

/**
 *  Třída PrikazZahod implementuje pro hru příkaz zahod.
 *  Tato třída je součástí jednoduché textové hry.
 *  
 *@author     Nikoleta Kocáková
 *@version    v1.0 pre semestrálnu prácu.
 */
public class PrikazZahod implements IPrikaz{
    private static final String NAZEV = "zahod";
    private HerniPlan plan;
    private Batoh batoh;
    
    /**
     *  Konstruktor třídy
     *  
     *  @param plan herní plán, ve kterém se bude ve hře "chodit"
     *  @param batoh batoh , ve kterem se nacházejí předmety.
     */    
     public PrikazZahod(HerniPlan plan , Batoh batoh) {
         this.plan = plan;
         this.batoh = batoh;
     }
     
     /**
     *  Provádí příkaz "zahod". Zkouší se zahodit daný předmět. Pokud předmět
     *  existuje, odstrani se předmět z batohu. Pokud zadaný předmět
     *  není, vypíše se chybové hlášení.
     *
     *@param parametry - jako  parametr obsahuje jméno ppředmětu,
     *                         který se má zahodit.
     *@return zpráva, kterou vypíše hra hráči
     */
     @Override
     public String provedPrikaz(String... parametry) {
         if (parametry.length == 0) {
             // pokud chybí druhé slovo (sousední prostor), tak ....
             return "Čo mám zahodiť? Musíš zadat jméno veci";
         }

         String zahodTentoPredmet = parametry[0]; //proste ked dam natvrdo nazov predmetu ktory sa ma odstranit tak sa odstrani , inak nie 

         // skúšame zobrať nasledujúcu vec do batohu
         Predmet predmet = batoh.vratPredmet(zahodTentoPredmet);

         
         if(batoh.obsahuje(predmet.getNazov()) == false){
             return "Predmet sa v batohu nenachádza.";
         }
         plan.getAktualniProstor().pridajPredmet(predmet);
         batoh.odoberPredmet(predmet);
         return "Predmet úspešne odstránený z batohu.";
    
     }
     
     
     /**
      *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
      *  
      *  @ return nazev prikazu
      */
     @Override
     public String getNazev() {
         return NAZEV;
     }
}

