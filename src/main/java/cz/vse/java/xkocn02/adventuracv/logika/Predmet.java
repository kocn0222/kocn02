/* UTF-8 codepage: Příliš žluťoučký kůň úpěl ďábelské ódy. ÷ × ¤
 * «Stereotype», Section mark-§, Copyright-©, Alpha-α, Beta-β, Smile-☺
 */
package cz.vse.java.xkocn02.adventuracv.logika;





/*******************************************************************************
 * Instance třídy {@code Predmet} představují predmety uložené
 * v místnostech v domě. Obsahuje také informaci o tom , jestli je 
 * předmet přenositelný nebo ne.
 *
 * @author  Nikoleta Kocáková
 * @version v1.0 pre semestrálnú prácu
 */
public class Predmet
{
    private HerniPlan plan;
    private String nazov;
    private boolean prenositelny;
    
    /**
     * Vytvoření předmětu se zadaným popisem a informaci o přenositelnosti.
     *
     * @param plan herni plan na kterem se hra odehráva , nazev nazev predmetu, jednoznačný identifikátor, jedno slovo nebo
     * víceslovný název bez mezer , prenositelny inforamce o prenositelnosti.
     * @param popis Popis prostoru.
     */
    public Predmet(String nazov , boolean prenositelny){
        this.plan = plan;
        this.nazov = nazov;
        this.prenositelny = prenositelny;
    }
    
    /**
     *  Metoda vrací název predmetu 
     *  
     *  @ return nazev predmetu
     */
    public String getNazov(){
        return nazov;
    }
    
    /**
     *  Metoda vrací informaci o přenositelnosti
     *  
     *  @ return prenositelny
     */
    public boolean getPrenositelny(){
        return prenositelny;
    }
    
    
}
