package cz.vse.java.xkocn02.adventuracv.logika;


/**
 *  Class HerniPlan - třída představující mapu a stav adventury.
 * 
 *  Tato třída inicializuje prvky ze kterých se hra skládá:
 *  vytváří všechny prostory, umístňuje v nich věci,
 *  propojuje je vzájemně pomocí východů 
 *  a pamatuje si aktuální prostor, ve kterém se hráč právě nachází.
 *
 *@author     Nikoleta Kocáková
 *@version    v1.0 pre semestrálnu prácu.
 */
public class HerniPlan {
    
    private boolean ohenObyvacka = true;
    private boolean rebrikPolozeny = false;
    private boolean dvereUzamknute = true;
    private boolean kyslikPoskytnuty = false;
    private Prostor aktualniProstor;
    
     /**
     *  Konstruktor který vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Jako výchozí aktuální prostor nastaví halu.
     */
    public HerniPlan() {
        zalozProstoryHry();

    }
    /**
     *  Vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Jako výchozí aktuální prostor nastaví domeček.
     */
    private void zalozProstoryHry() {
        // vytvářejí se jednotlivé prostory
        Prostor kuchyna = new Prostor("kuchyňa","kuchyňa v byte kde býva babička.");
        Prostor predsien = new Prostor("predsieň", "predsieň v byte kde býva babička.");
        Prostor obyvacka = new Prostor("obývačka","obývačka bytu kde býva babička.");
        Prostor chodba = new Prostor("chodba","chodba v byte kde býva babička.");
        Prostor schody = new Prostor("schody","schody v byte kde býva babička.");
        Prostor chodbaDva = new Prostor("chodbaDva","druhá chodba v byte kde býva babička");
        Prostor izba = new Prostor("izba","izba v byte kde býva babička");
        
        // přiřazují se průchody mezi prostory (sousedící prostory)
        kuchyna.setVychod(predsien);
        predsien.setVychod(kuchyna);
        predsien.setVychod(obyvacka);
        obyvacka.setVychod(predsien);
        obyvacka.setVychod(chodba);
        chodba.setVychod(obyvacka);
        chodba.setVychod(schody);
        schody.setVychod(chodba); 
        schody.setVychod(chodbaDva);
        chodbaDva.setVychod(schody);
        chodbaDva.setVychod(izba);
        izba.setVychod(chodbaDva);
        
        // vytvářejí se jednotlivé předmety
        Predmet ventilator = new Predmet( "ventilator" , true);
        Predmet rebrik = new Predmet( "rebrik" , true);
        Predmet kluc = new Predmet("kluc", true);
        Predmet kyslik = new Predmet( "kyslik" , true);
        Predmet lampa = new Predmet( "lampa" , false);
        Predmet ladnicka = new Predmet( "chladnicka" , false);
        Predmet sekera = new Predmet( "sekera" , true);
        Predmet babicka = new Predmet( "babička" , true);
        Predmet raketa = new Predmet( "raketa" , true);
        
        //pridat predmety do danych priestorov
        kuchyna.pridajPredmet(ladnicka);
        obyvacka.pridajPredmet(ventilator);
        chodba.pridajPredmet(kluc);
        chodba.pridajPredmet(rebrik);
        chodbaDva.pridajPredmet(kyslik);
        chodbaDva.pridajPredmet(sekera);
        predsien.pridajPredmet(lampa);
        izba.pridajPredmet(babicka);
        
        aktualniProstor = predsien;  // hra začíná v predsieni      
    }
    
    /**
     *  Metoda vrací odkaz na aktuální prostor, ve ktetém se hráč právě nachází.
     *
     *@return     aktuální prostor
     */
    
    public Prostor getAktualniProstor() {
        return aktualniProstor;
    }
    
    /**
     *  Metoda nastaví aktuální prostor, používá se nejčastěji při přechodu mezi prostory
     *
     *@param  prostor nový aktuální prostor
     */
    public void setAktualniProstor(Prostor prostor) {
       aktualniProstor = prostor;
    }
    
    /**
     *  Metoda vrací odkaz na proměnnou rebrikPolozeny, ktorá obsahuje informaci jestli je možné přejít cez prostor schody.
     *
     *@return     rebrikPolozeny
     */
    public boolean getRebrikPolozeny() {
        return rebrikPolozeny;
    }
    
    /**
     *  Metoda nastaví hodnotu proměnné polozRebrik na true, používá se při pokládaní řebříku na schody.
     *
     */
    public void polozRebrik() {
        rebrikPolozeny = true;
    }
    
      /**
     *  Metoda vrací odkaz na proměnnou ohenObyvacka, ktorá obsahuje informaci jestli je oheň v obývačce.
     *
     *@return     ohenObyvacka
     */
    public boolean getOhenObyvacka() {
        return ohenObyvacka;
    }
    
    /**
     *  Metoda nastaví hodnotu proměnné polozRebrik na false, používá se při hasení ohně v prostoru obývačka.
     *
     */
    public void zahasOhenObyvacka() {
        ohenObyvacka = false;
    }
    
     /**
     *  Metoda vrací odkaz na proměnnou dvereUzamknute, ktorá obsahuje informaci jestli jsou dveře do prostoru izba uzamknuté.
     *
     *@return     dvereUzamknute
     */
    public boolean getDvereUzamknute() {
        return dvereUzamknute;
    }
    
    /**
     *  Metoda nastaví hodnotu proměnné dvereUzamknute na false, používá se při odemknutí ddveří v prostoru izba.
     *
     */
    public void odomkniDvere() {
        dvereUzamknute = false;
    }
    
    /**
     *  Metoda vrací odkaz na proměnnou kyslikPoskytnuty, ktorá obsahuje informaci jestli byl kyslik poskytnuty babičce.
     *
     *@return     kyslikPoskytnuty
     */
    public boolean getKyslikPoskytnuty() {
        return kyslikPoskytnuty;
    }
    
    /**
     *  Metoda nastaví hodnotu proměnné poskytniKyslik na true, používá se při poskytování kyslíku babičce.
     *
     */
    public void poskytniKyslik() {
        kyslikPoskytnuty = true;
    }
    
}
