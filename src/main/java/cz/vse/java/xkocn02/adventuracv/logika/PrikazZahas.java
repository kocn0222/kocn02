package cz.vse.java.xkocn02.adventuracv.logika;

/**
 *  Třída PrikazZahas implementuje pro hru příkaz zahas.
 *  Tato třída je součástí jednoduché textové hrya umožňuje zahasit oheň 
 *  pomocí hasíciho nástroje.
 *  
 *@author     Nikoleta Kocáková
 *@version    v1.0 pre semestrálnu prácu.
 */
public class PrikazZahas implements IPrikaz{
    private static final String NAZEV = "zahas";
    private HerniPlan plan;
    private Batoh batoh;
    
    /**
     *  Konstruktor třídy
     *  
     *  @param plan herní plán, ve kterém se bude ve hře "chodit" 
     *  @param batoh batoh , ve kterem se nacházejí předmety.
     */    
     public PrikazZahas(HerniPlan plan , Batoh batoh) {
         this.plan = plan;
         this.batoh = batoh;
     }
     
      /**
     *  Provádí příkaz "zahas". Zkouší se zahasit oheň v prostoru obývačka. Pokud je
     *  aktuální prostor roven "obývačka" a batoh obsahuje předmět "hasicsky_pristroj", 
     *  oheň se uhasí. 
     *
     *@return zpráva, kterou vypíše hra hráči
     */ 
     @Override
     public String provedPrikaz(String... parametry) {
         if(plan.getAktualniProstor().getNazev()== "obývačka") {
             if(batoh.obsahuje("hasicsky_pristroj") == true) {
                if(plan.getOhenObyvacka() == true) {
                    plan.zahasOhenObyvacka();
                }
                else {
                    return "Oheň je už zahasený.";
                }
             }
         }
         else {
            return "V tejto miestnosti nehorí";
         }
         Predmet hasicak = batoh.vratPredmet("hasicsky_pristroj");
         batoh.odoberPredmet(hasicak);
         return "Oheň ste úspešne zahasili.";
     }
     
     
     /**
      *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
      *  
      *  @ return nazev prikazu
      */
     @Override
     public String getNazev() {
         return NAZEV;
     }
}