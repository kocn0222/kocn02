package cz.vse.java.xkocn02.adventuracv.logika;

/**
 *  Třída PrikazJdi implementuje pro hru příkaz jdi.
 *  Tato třída je součástí jednoduché textové hry.
 *  
 *@author     Nikoleta Kocáková
 *@version    v1.0 pre semestrálnu prácu.
 */
class PrikazJdi implements IPrikaz {
    private static final String NAZEV = "jdi";
    private HerniPlan plan;
    
    /**
    *  Konstruktor třídy
    *  
    *  @param plan herní plán, ve kterém se bude ve hře "chodit" 
    */    
    public PrikazJdi(HerniPlan plan) {
        this.plan = plan;
    }

    /**
     *  Provádí příkaz "jdi". Zkouší se vyjít do zadaného prostoru. Pokud prostor
     *  existuje, vstoupí se do nového prostoru. Pokud zadaný sousední prostor
     *  (východ) není, vypíše se chybové hlášení.
     *
     *@param parametry - jako  parametr obsahuje jméno prostoru (východu),
     *                         do kterého se má jít.
     *@return zpráva, kterou vypíše hra hráči
     */ 
    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            // pokud chybí druhé slovo (sousední prostor), tak ....
            return "Kam mám jít? Musíš zadat jméno východu";
        }

        String smer = parametry[0];

        // zkoušíme přejít do sousedního prostoru
        Prostor sousedniProstor = plan.getAktualniProstor().vratSousedniProstor(smer);

        if (sousedniProstor == null) {
            return "Tam se odsud jít nedá!";
        }
        else if(sousedniProstor.getNazev() == "chodba") {
            if(plan.getOhenObyvacka() == true) {
                return "Kvôli ohňu sa nedá dostať do chodby.";
            }
        }
        else if(sousedniProstor.getNazev() == "chodbaDva") {
            if(plan.getAktualniProstor().getNazev() == "schody" && plan.getRebrikPolozeny() == false) {
                return "Schody sú neprechodné kvôli ohňu , je nutné ich obísť s pomocou predmetu";
            }
        }
        else if(sousedniProstor.getNazev() == "izba") {
            if(plan.getDvereUzamknute() == true) {
                return "Dvere do izby sú zamknuté.";
            }
        }
        
        plan.setAktualniProstor(sousedniProstor);
        return sousedniProstor.dlouhyPopis();
    }
    
    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *  
     *  @ return nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }
}


