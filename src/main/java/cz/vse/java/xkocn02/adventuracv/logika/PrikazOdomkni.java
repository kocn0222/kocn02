package cz.vse.java.xkocn02.adventuracv.logika;

/**
 *  Třída PrikazOdomkni implementuje pro hru příkaz odomkni.
 *  Tato třída je součástí jednoduché textové hry a umožňuje hráči odomknout dveře.
 *  
 *@author     Nikoleta Kocáková
 *@version    v1.0 pre semestrálnu prácu.
 */
public class PrikazOdomkni implements IPrikaz{
    
    private static final String NAZEV = "odomkni";
    private HerniPlan plan;
    private Batoh batoh;
    
    /**
     *  Konstruktor třídy
     *  
     *  @param plan herní plán, ve kterém se bude ve hře "chodit" 
     *  @param batoh batoh , ve kterem se nacházejí předmety.
     */    
     public PrikazOdomkni(HerniPlan plan , Batoh batoh) {
         this.plan = plan;
         this.batoh = batoh;
     }
     
     /**
     *  Provádí příkaz "odomkni". Zkouší se odemknout dveře do prostoru "izba". Pokud je
     *  aktuální prostor roven "chodbaDva" a batoh obsahuje předmět "kluc", 
     *  dveře se odemknou. 
     *
     *@return zpráva, kterou vypíše hra hráči
     */ 
     @Override
     public String provedPrikaz(String... parametry) {
         if(plan.getAktualniProstor().getNazev()== "chodbaDva") {
             if(batoh.obsahuje("kluc") == true) {
                if(plan.getDvereUzamknute() == true) {
                    plan.odomkniDvere();
                }
   
             }else {
                 return "Aby ste mohli odomknúť dvere , potrebujete kľúč";
             }
         }
         else {
             return "V tejto miestnosti sa nedá nič odomknúť.";
         }
         
         Predmet kluc = batoh.vratPredmet("kluc");
         batoh.odoberPredmet(kluc);
         return "Úspešne ste odomkli dvere";
     }
     
     
     /**
      *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
      *  
      *  @ return nazev prikazu
      */
     @Override
     public String getNazev() {
         return NAZEV;
     }
}
