package cz.vse.java.xkocn02.adventuracv.logika;

/**
 *  Třída PrikazLez implementuje pro hru příkaz lez.
 *  Tato třída je součástí jednoduché textové hry a umožňuje lézt po řebříku.
 *  
 *@author     Nikoleta Kocáková
 *@version    v1.0 pre semestrálnu prácu.
 */
public class PrikazLez implements IPrikaz{
    
    private static final String NAZEV = "lez";
    private HerniPlan plan;
    private Batoh batoh;
    
    /**
     *  Konstruktor třídy
     *  
     *  @param plan herní plán, ve kterém se bude ve hře "chodit" 
     *  @param batoh batoh , ve kterem se nacházejí předmety.
     */    
     public PrikazLez(HerniPlan plan , Batoh batoh) {
         this.plan = plan;
         this.batoh = batoh;
     }
     
     /**
     *  Provádí příkaz "lez". Zkouší se přelézt cez schody. Pokud je
     *  aktuální prostor roven "schody" a batoh obsahuje předmět "rebrik", 
     *  pak je možné přelézt po řebříku do další místnosti. 
     *
     *@return zpráva, kterou vypíše hra hráči
     */ 
     @Override
     public String provedPrikaz(String... parametry) {
         if(plan.getAktualniProstor().getNazev()== "schody") {
             if(batoh.obsahuje("rebrik") == true) {
                if(plan.getRebrikPolozeny() == false) {
                    plan.polozRebrik();
                }
                else {
                    return "Rebrík je už položený.";
                }
             }else {
                 return "Aby ste sa mohli dostať cez oheň potrebujete rebrík.";
             }
         }
         else {
            return "V tejto miestnosti nemusíte liezť.";
         }
         Predmet rebrik = batoh.vratPredmet("rebrik");
         batoh.odoberPredmet(rebrik);
         return "Úspešne ste preliezli cez schody.";
     }
     
     
     /**
      *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
      *  
      *  @ return nazev prikazu
      */
     @Override
     public String getNazev() {
         return NAZEV;
     }
}