package cz.vse.java.xkocn02.adventuracv.logika;

/**
 *  Třída Hra - třída představující logiku adventury.
 * 
 *  Toto je hlavní třída  logiky aplikace.  Tato třída vytváří instanci třídy HerniPlan, která inicializuje mistnosti hry
 *  a vytváří seznam platných příkazů a instance tříd provádějící jednotlivé příkazy.
 *  Vypisuje uvítací a ukončovací text hry.
 *  Také vyhodnocuje jednotlivé příkazy zadané uživatelem.
 *
 *@author     Nikoleta Kocáková
 *@version    v1.0 pre semestrálnú prácu
 */

public class Hra implements IHra {
    private SeznamPrikazu platnePrikazy;    // obsahuje seznam přípustných příkazů
    private HerniPlan herniPlan;
    private Batoh batoh;
    private boolean konecHry = false;

    /**
     *  Vytváří hru a inicializuje místnosti (prostřednictvím třídy HerniPlan) a seznam platných příkazů.
     */
    public Hra() {
        herniPlan = new HerniPlan();
        this.batoh = new Batoh();
        platnePrikazy = new SeznamPrikazu();
        platnePrikazy.vlozPrikaz(new PrikazNapoveda(platnePrikazy));
        platnePrikazy.vlozPrikaz(new PrikazJdi(herniPlan));
        platnePrikazy.vlozPrikaz(new PrikazKonec(this));
        platnePrikazy.vlozPrikaz(new PrikazVezmi(herniPlan , batoh));
        platnePrikazy.vlozPrikaz(new PrikazZahas(herniPlan , batoh));
        platnePrikazy.vlozPrikaz(new PrikazLez(herniPlan , batoh));
        platnePrikazy.vlozPrikaz(new PrikazZahod(herniPlan , batoh));
        platnePrikazy.vlozPrikaz(new PrikazOdomkni(herniPlan , batoh));
        platnePrikazy.vlozPrikaz(new PrikazDajKyslik(herniPlan , batoh));
        platnePrikazy.vlozPrikaz(new PrikazBatoh(herniPlan , batoh));
    }

    /**
     *  Vrátí úvodní zprávu pro hráče.
     */
    public String vratUvitani() {
        return "Vitajte!\n" +
               "Na ulici Roosveltova 47, Praha býva babička, ktorá piekla jablčník.\n" +
               "Babka zaspala a zabudla vypnúť pec. Suseda zacítila dym a videla oheň tak rýchlo privolala hasičov.\n" +
               "Hráčom je požiarník, ktorý sa nachádza v predsieni horiaceho domu.\n" + 
               "Požiarník má so sebou len ochranný  odev, batoh a hasiací prístroj.\n"+ 
               "Jeho úlohou je dostať sa na 2. poschodie domu a zachrániť babičku v izbe, ktorá sa dusí.\n"+ 
               "Požiarnik musí prekonáť rôzne prekážky ako napríklad horiace schody, či nedostatok kyslíka.\n"+ 
               "K záchrane musí využiť objekty v dome.\n\n" +
               
               herniPlan.getAktualniProstor().dlouhyPopis()+
               "\nPredmety v miestnosti: " + herniPlan.getAktualniProstor().vratSeznamPredmetu() +
               "\n===============================================================";
    }
    
    /**
     *  Vrátí závěrečnou zprávu pro hráče.
     */
    public String vratEpilog() {
        return "Babičku ste úspešne zachránili z horiaceho domu.\n Ďakujem, že ste hrali moju hru :) ";
    }
    
    /** 
     * Vrací true, pokud hra skončila.
     */
     public boolean konecHry() {
        return konecHry;
    }

    /**
     *  Metoda zpracuje řetězec uvedený jako parametr, rozdělí ho na slovo příkazu a další parametry.
     *  Pak otestuje zda příkaz je klíčovým slovem  např. jdi.
     *  Pokud ano spustí samotné provádění příkazu.
     *
     *@param  radek  text, který zadal uživatel jako příkaz do hry.
     *@return          vrací se řetězec, který se má vypsat na obrazovku
     */
     public String zpracujPrikaz(String radek) {

        String[]slova = radek.split("[ \t]+");
        String slovoPrikazu = slova[0];
        String []parametry = new String[slova.length-1];
        for(int i=0 ;i<parametry.length;i++){
            parametry[i]= slova[i+1];   
        }
        String textKVypsani=" .... ";
        if (platnePrikazy.jePlatnyPrikaz(slovoPrikazu)) {
            IPrikaz prikaz = platnePrikazy.vratPrikaz(slovoPrikazu);
            textKVypsani = prikaz.provedPrikaz(parametry); 
        }
        else {
            textKVypsani="Nevím co tím myslíš? Tento příkaz neznám. ";
        }
        if(batoh.obsahuje("babička") == true && herniPlan.getKyslikPoskytnuty() == true) {
             this.setKonecHry(true);
         }
        return textKVypsani;
    }
    
    
     /**
     *  Nastaví, že je konec hry, metodu využívá třída PrikazKonec,
     *  mohou ji použít i další implementace rozhraní Prikaz.
     *  
     *  @param  konecHry  hodnota false= konec hry, true = hra pokračuje
     */
    void setKonecHry(boolean konecHry) {
        this.konecHry = konecHry;
    }
    
     /**
     *  Metoda vrátí odkaz na herní plán, je využita hlavně v testech,
     *  kde se jejím prostřednictvím získává aktualní místnost hry.
     *  
     *  @return     odkaz na herní plán
     */
     public HerniPlan getHerniPlan(){
        return herniPlan;
     }
    
}

