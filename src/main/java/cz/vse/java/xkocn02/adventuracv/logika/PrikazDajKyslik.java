package cz.vse.java.xkocn02.adventuracv.logika;

/**
 *  Třída PrikazDajKyslik implementuje pro hru příkaz dajKyslik.
 *  Tato třída je součástí jednoduché textové hry a umožňuje zachrániť babičku.
 *  
 *@author     Nikoleta Kocáková
 *@version    v1.0 pre semestrálnu prácu.
 */
public class PrikazDajKyslik implements IPrikaz{
    
    private static final String NAZEV = "dajKyslik";
    private HerniPlan plan;
    private Batoh batoh;
    
    /**
     *  Konstruktor třídy
     *  
     *  @param plan herní plán, ve kterém se bude ve hře "chodit" 
     *  @param batoh batoh , ve kterem se nacházejí předmety.
     */    
     public PrikazDajKyslik(HerniPlan plan , Batoh batoh) {
         this.plan = plan;
         this.batoh = batoh;
     }
     
     /**
     *  Provádí příkaz "dajKyslik". Zkouší se poskytnout kyslík babičce. Pokud je
     *  aktuální prostor roven "izba" a batoh obsahuje předmět "kyslik", 
     *  babičce se kyslík poskytne. 
     *
     *@return zpráva, kterou vypíše hra hráči
     */ 
     @Override
     public String provedPrikaz(String... parametry) {
         if(plan.getAktualniProstor().getNazev()== "izba") {
             if(batoh.obsahuje("kyslik") == true) {
                if(plan.getKyslikPoskytnuty() == false) {
                    plan.poskytniKyslik();
                }else {
                    return "Kyslík už bol babičke poskytnutý";
                }
             }else {
                 return "Potrebujete kyslik aby ste babičku zachránili.";
             }
         }
         else {
            return "V tejto miestnosti nemôžete nikomu poskytnúť kyslík.";
         }
         
         Predmet kyslik = batoh.vratPredmet("kyslik");
         batoh.odoberPredmet(kyslik);
         return "Úspešne ste poskytli babičke kyslík aby sa neudusila.";
     }
     
     
     /**
      *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
      *  
      *  @ return nazev prikazu
      */
     @Override
     public String getNazev() {
         return NAZEV;
     }
}


