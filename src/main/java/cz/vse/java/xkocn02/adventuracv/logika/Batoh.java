/* UTF-8 codepage: Příliš žluťoučký kůň úpěl ďábelské ódy. ÷ × ¤
 * «Stereotype», Section mark-§, Copyright-©, Alpha-α, Beta-β, Smile-☺
 */
package cz.vse.java.xkocn02.adventuracv.logika;
import java.util.*;
import java.util.stream.Collectors;




/*******************************************************************************
 * Instance třídy {@code Batoh} představují batoh(inventár)
 * do kterého je možné ukládat omezené množství předmetú(max 3).
 * Obsahuje také metodu pro získaní obsahu batohu v textové podobě.
 * 
 * @author  Nikoleta Kocáková
 * @version v1.0 pre semestrálnú prácu
 */
public class Batoh
{
    
    private Map<String,Predmet> seznamVeci;
    private int limit;
    
    /**
    *  Konstruktor třídy Batoh, který slouži k přenášení předmetú.
    *  V batohu se bude na začátku hry vyskytovat předmet hasicak.
    *  
    *  @param plan herní plán, ve kterém se bude ve hře "chodit"
    *  
    */  
    public Batoh(){
        this.limit = 3;
        seznamVeci = new HashMap();
        Predmet hasicak = new Predmet("hasicsky_pristroj" , true);
        this.pridajPredmet(hasicak);

    }
    
    /**
    *  Metoda , která slouži na přidání předmetu do batohu.
    *  
    *  @param p představuje předmet , který chceme do batohu přidat
    *  
    */  
    public boolean pridajPredmet(Predmet p){
        if(this.getSize() < 3){
            seznamVeci.put(p.getNazov() , p);
            return true;
        }
        return false;
    }
    
    /**
    *  Metoda , která slouži na vrátení celého seznamu věcí v batohu.
    *  
    *  @return seznamVeci
    */  
    public Map<String , Predmet> getSeznamVeci(){
        return this.seznamVeci;
    }
    
    /**
    *  Metoda , která slouži na odebíraní předmetu z batohu.
    *  
    *  @param p představuje předmet , který chceme z batohu odstranit.
    *  
    */ 
    public boolean odoberPredmet(Predmet p){
        if(obsahuje(p.getNazov())){
            seznamVeci.remove(p.getNazov());
            return true;
        }
        return false;
    }
    
    /**
    *  Metoda , která slouži na získaní velikosti seznamu věcí v batohu.
    * 
    *  @return size
    */ 
    public int getSize(){
        return seznamVeci.size();
    }
    
    /**
    *  Metoda , která slouži na získaní informace , ktorou použijeme při zjisťování jestli se daný 
    *  předmet v batohu nachází nebo ne.
    *  
    *  @param p představuje textovou charakteristiku předmetu.
    *  @return boolean
    */ 
    public boolean obsahuje(String p){
        return seznamVeci.containsKey(p);
    }   
    
    /**
    *  Metoda , která slouži pro získávaní předmetu z batohu.
    *  
    *  @param String predmet představuje textovou charakteristiku předmetu.
    *  @return Predmet
    */ 
    public Predmet vratPredmet(String predmet){
        if(obsahuje(predmet)){
            return seznamVeci.get(predmet);
        }
        return null;
    }
   
    /**
    *  Metoda , která slouži pro získávaní textové reprezentace obsahu batohu.
    *  
    *  @return String ret (popis batohu)
    */
    public String getObsahBatohu(){
        if(seznamVeci.size() == 0 ){
            return "Batoh je prázdny.";
        }
        String ret = "";
        for(String nazov : seznamVeci.keySet()){
            ret += nazov + " ";
        }
        return ret;
    }
}
