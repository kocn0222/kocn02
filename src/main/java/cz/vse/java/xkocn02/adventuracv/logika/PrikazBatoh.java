package cz.vse.java.xkocn02.adventuracv.logika;

/**
 *  Třída PrikazBatoh implementuje pro hru příkaz batoh.
 *  Tato třída je součástí jednoduché textové hry a umožňuje náhled do batohu.
 *  
 *@author     Nikoleta Kocáková
 *@version    v1.0 pre semestrálnu prácu.
 */
public class PrikazBatoh implements IPrikaz{
    private static final String NAZEV = "batoh";
    private HerniPlan plan;
    private Batoh batoh;
    
    /**
     *  Konstruktor třídy
     *  
     *  @param plan herní plán, ve kterém se bude ve hře "chodit"
     *  @param batoh batoh , ve kterem se nacházejí předmety.
     */    
     public PrikazBatoh(HerniPlan plan , Batoh batoh) {
         this.plan = plan;
         this.batoh = batoh;
     }
     
     /**
     *  Provádí příkaz "batoh". Vypíše se obsah batohu.
     *  
     *  @return zpráva, kterou vypíše hra hráči
     */ 
     @Override
     public String provedPrikaz(String... parametry) {
         return batoh.getObsahBatohu();
     }
     
     
     /**
      *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
      *  
      *  @ return nazev prikazu
      */
     @Override
     public String getNazev() {
         return NAZEV;
     }
}

