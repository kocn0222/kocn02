package cz.vse.java.xkocn02.adventuracv.uitext;

import java.io.*;
import java.util.Scanner;
import cz.vse.java.xkocn02.adventuracv.logika.IHra;
/**
 *  Class TextoveRozhrani
 * 
 *  Toto je uživatelského rozhraní aplikace Adventura
 *  Tato třída vytváří instanci třídy Hra, která představuje logiku aplikace.
 *  Čte vstup zadaný uživatelem a předává tento řetězec logice a vypisuje odpověď logiky na konzoli.
 *  
 *  
 *
 *@author     Michael Kolling, Lubos Pavlicek, Jarmila Pavlickova
 *@version    pro školní rok 2016/2017
 */

public class TextoveRozhrani {
    private IHra hra;

    /**
     *  Konstruktor - Vytváří hru.
     */
    public TextoveRozhrani(IHra hra) {
        this.hra = hra;
    }

    /**
     *  Hlavní metoda hry. Vypíše úvodní text a pak opakuje čtení a zpracování
     *  příkazu od hráče do konce hry (dokud metoda konecHry() z logiky nevrátí
     *  hodnotu true). Nakonec vypíše text epilogu.
     */
    public void hraj() {
        System.out.println(hra.vratUvitani());

        // základní cyklus programu - opakovaně se čtou příkazy a poté
        // se provádějí do konce hry.

        while (!hra.konecHry()) {
            String radek = prectiString();
            System.out.println(hra.zpracujPrikaz(radek));
            System.out.println("Predmety v miestnosti: " + hra.getHerniPlan().getAktualniProstor().vratSeznamPredmetu());
            System.out.println(hra.getHerniPlan().getAktualniProstor().popisVychodu());
            System.out.println("Nachádzate sa v priestore " + hra.getHerniPlan().getAktualniProstor().getNazev());
            System.out.println("Ak neviete aké príkazy použiť , zadajte príkaz nápověda");
            System.out.println("===============================================================");
        }

        System.out.println(hra.vratEpilog());
    }
    
    public void hrajZeSuboru(File subor) {
        try (BufferedReader citacka= new BufferedReader (new FileReader(subor))) //import java.IO.*
    {
        System.out.println(hra.vratUvitani());

        // základní cyklus programu - opakovaně se čtou příkazy a poté
        // se provádějí do konce hry.
        String radek = citacka.readLine();
        while (!hra.konecHry() && radek != null) {
            System.out.println("***"+radek+"**");
            System.out.println(hra.zpracujPrikaz(radek));
            radek = citacka.readLine();
            
        }

        System.out.println(hra.vratEpilog());
    }
        
        catch (FileNotFoundException e){
        System.out.println ("subor s prikazom nenalezen");
        }
        
        catch (IOException e){
        System.out.println ("chzba pri praci se suorom");
        }
    }
    
    /**
     *  Metoda přečte příkaz z příkazového řádku
     *
     *@return    Vrací přečtený příkaz jako instanci třídy String
     */
    private String prectiString() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("> ");
        return scanner.nextLine();
    }

}
