package cz.vse.java.xkocn02.adventuracv.logika;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/*******************************************************************************
 * Testovací třída HraTest slouží ke komplexnímu otestování
 * třídy Hra
 *
 * @author    Jarmila Pavlíčková
 * @version  pro školní rok 2016/2017
 */
public class HraTest {
    private Hra hra1;

    //== Datové atributy (statické i instancí)======================================

    //== Konstruktory a tovární metody =============================================
    //-- Testovací třída vystačí s prázdným implicitním konstruktorem ----------

    //== Příprava a úklid přípravku ================================================

    /***************************************************************************
     * Metoda se provede před spuštěním každé testovací metody. Používá se
     * k vytvoření tzv. přípravku (fixture), což jsou datové atributy (objekty),
     * s nimiž budou testovací metody pracovat.
     */
    @Before
    public void setUp() {
        hra1 = new Hra();
    }

    /***************************************************************************
     * Úklid po testu - tato metoda se spustí po vykonání každé testovací metody.
     */
    @After
    public void tearDown() {
    }

    //== Soukromé metody používané v testovacích metodách ==========================

    //== Vlastní testovací metody ==================================================

    /***************************************************************************
     * Testuje průběh hry, po zavolání každěho příkazu testuje, zda hra končí
     * a v jaké aktuální místnosti se hráč nachází.
     * Při dalším rozšiřování hry doporučujeme testovat i jaké věci nebo osoby
     * jsou v místnosti a jaké věci jsou v batohu hráče.
     * 
     */
    @Test
    public void testPrubehHry() {
        assertEquals("predsieň", hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz("jdi obývačka");
        assertEquals(false, hra1.konecHry());
        assertEquals("obývačka", hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz("jdi chodba");
        assertEquals(false, hra1.konecHry());
        assertEquals("obývačka", hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz("konec");
        assertEquals(true, hra1.konecHry());
 
    }

    @Test 
    public void testVyhra(){
        hra1.zpracujPrikaz("jdi obývačka");
        hra1.zpracujPrikaz("zahas");
        hra1.zpracujPrikaz("jdi chodba");
        hra1.zpracujPrikaz("vezmi kluc");
        hra1.zpracujPrikaz("vezmi rebrik");
        hra1.zpracujPrikaz("jdi schody");
        hra1.zpracujPrikaz("lez");
        hra1.zpracujPrikaz("jdi chodbaDva");
        hra1.zpracujPrikaz("vezmi kyslik");
        hra1.zpracujPrikaz("odomkni");
        hra1.zpracujPrikaz("jdi izba");
        hra1.zpracujPrikaz("dajKyslik");
        hra1.zpracujPrikaz("vezmi babička");
        
        assertEquals(true , hra1.konecHry());
        
    }
    
    @Test
    public void testProhra(){
        hra1.zpracujPrikaz("jdi obývačka");
        hra1.zpracujPrikaz("jdi chodba");
        hra1.zpracujPrikaz("vezmi kluc");
        hra1.zpracujPrikaz("vezmi rebrik");
        hra1.zpracujPrikaz("jdi schody");
        hra1.zpracujPrikaz("lez");
        hra1.zpracujPrikaz("jdi chodbaDva");
        hra1.zpracujPrikaz("vezmi kyslik");
        hra1.zpracujPrikaz("odomkni");
        hra1.zpracujPrikaz("jdi izba");
        hra1.zpracujPrikaz("dajKyslik");
        hra1.zpracujPrikaz("vezmi babička");
        
        assertEquals(false , hra1.konecHry());
    }
}
