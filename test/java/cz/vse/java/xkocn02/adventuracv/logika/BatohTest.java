package cz.vse.java.xkocn02.adventuracv.logika;



import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Testovacia trieda {@code BatohTest}.
 *
 * @author   «meno autora»
 * @version  «verzia alebo dátum»
 */
public class BatohTest
{
    Batoh batoh = new Batoh();
    private Predmet p1 = new Predmet("stolicka" , true);
    private Predmet p2 = new Predmet("stol" , false);
    private Predmet p3 = new Predmet("lampa" , true);
    private Predmet p4 = new Predmet("obraz", true);
 
    /**
     * Inicializácia predchádzajúca spusteniu každého testu a pripravujúca
     * tzv. prípravok (fixture), čo je množina objektov, s ktorými budú
     * testy pracovať.
     */
    @Before
    public void setUp()
    {
   
    }
    
    @Test
    public void testJdePridatDoBatohu()
    {
        //Prenositelny predmet
        assertEquals(true ,batoh.pridajPredmet(p1));
        assertEquals(true ,batoh.pridajPredmet(p3));
       
    }
    
    @Test
    public void testBatohLimit()
    {
        assertEquals(true ,batoh.pridajPredmet(p1));
        assertEquals(true ,batoh.pridajPredmet(p3));
        assertEquals(false ,batoh.pridajPredmet(p4));

    }
    
    @Test
    public void testOdoberanieZBatohu(){
        batoh.pridajPredmet(p1);
        assertEquals(2 , batoh.getSize());
        batoh.odoberPredmet(p1);
        assertEquals(1 , batoh.getSize());
    }
    
    /**
     * „Upratovanie“ po teste – táto metóda je spustená po vykonaní každého
     * testu.
     */
    @After
    public void tearDown()
    {
    }
}
